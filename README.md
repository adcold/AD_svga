# AD_SVGA

#### 介绍
svga礼物特效

#### 软件架构
AE编辑导出SVGA文件


#### 安装教程

1.  [UniApp插件_SDK](https://ext.dcloud.net.cn/plugin?id=1711)
2.  [原生IOS_SDK](https://github.com/svga/SVGAPlayer-iOS)
3.  [原生安卓_SDK](https://github.com/svga/SVGAPlayer-Android)
4.  [WEB插件_SDK](https://github.com/svga/SVGAPlayer-Web)
5.  [微信小程序_SDK](https://github.com/svga/SVGAPlayer-Web/tree/mp)

#### 使用说明

1.  [在线预览](http://svga.io/svga-preview.html) 各端使用方法请百度谷歌gitHub

#### 版权说明

1.  本仓库动画禁止商用,因为可以涉及版权问题,部分动画与素材是有版权的,部分素材个人出资购买了版权
2.  有版权或者无版权会进行编著

#### 打赏AD

1.  素未谋面的小姐姐小哥哥们大家好,本仓库完全免费开源有的素材视频是自己花钱买的如果想要更多的好礼物请打赏AD吧

